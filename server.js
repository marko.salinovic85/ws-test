const WebSocket = require('ws')
const PORT = 5000;

const wsServer = new WebSocket.Server({port: PORT})

wsServer.on('connection', function(socket) {
    console.log('Client has connected')

    socket.on('message', function(msg) {
        wsServer.clients.forEach(function (client){
            client.send('Netko je rekao: ' + msg)
        })
    })
})

console.log('Server is listening on port ' + PORT)